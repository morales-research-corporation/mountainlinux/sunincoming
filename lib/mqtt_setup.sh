#!/bin/bash

# Location of the git.centos.org CA, cert, and private key - used for authenticating to their queue
# These are expressed as full arguments to the mosquitto_sub process:
export CENTOS_GIT_CREDS="--cafile ~/.centos-server-ca.cert --cert ~/.centos.cert --key ~/.centos.cert"


