import time, os
from datetime import datetime

class html_out:

# Write out a pretty html file with table and jquery/datatables

  def __init__(self, tableData):
  
    # Current timestamp
    theTime = datetime.now().strftime("%Y-%m-%d  %H:%M:%S")
  
    # HTML template file should be in this same lib/ folder
    templateHtml = os.path.join(os.path.dirname(__file__), 'html_out.html')
  
  
    # Load HTML template into variable, simply swap timestamp and tableData with the real deal
    # (then send it to STDOUT):
    f = open(templateHtml, 'r')
    htmlData = f.read()
    f.close()
    
     
    htmlData = htmlData.replace("@@TIMESTAMP@@", theTime)
    htmlData = htmlData.replace("@@TABLE_DATA@@", tableData)
  
    print(htmlData)
  
  
