# global variables that the git_monitor program uses:
# 
# This is a (default) Rocky 8 oriented template.  See other setup files in this folder for Rocky 9 (or other versions) examples
#
#

# Prefixes are the URL before "rpms/RPMNAME"
ROCKY_PREFIX = "https://git.rockylinux.org/staging/"

UPSTREAM_PREFIX = "https://git.centos.org/"

# Comma-separated list (no spaces) of Rocky DNF repos to query against
# (to match RHEL import tags <----> Rocky packages
DNF_REPOS="Rocky8_BaseOS_Source,Rocky8_AppStream_Source,Rocky8_PowerTools_Source,Rocky8_extras_Source,Rocky8_plus_Source,Rocky8_HighAvailability_Source,Rocky8_ResilientStorage_Source,Rocky8_RT_Source"


# List of allowed branch patterns - these are strings which, if found in a branchname, we want to process that commit!
# Remember that if *any* part of this string appears in a branch, then it will be detected!
# Example:  '/c8'  will count for branch '/c8', but will also count for branch 'c8-stream-2.5'
allowed_branches = ['/c8', '/c8-']


# List of DISallowed branch patterns - if we find one of these in a branch name, we WILL NOT process that commit!
# Disallowed branch patterns will ALWAYS TRUMP the allowed ones.
# Example:  the branch for a commit is '/c8s-stream-2.5'.  It contains the allowed pattern "/c8", but contains the disallowed pattern "/c8s" .
# Disallowed always wins, and this commit will not be processed.
disallowed_branches = ['/c8s', '/c8-beta', '/c8-container']



# DNF Repofile to use
# This is a big string representing a .repo file that contains the repositories listed above (DNF_REPOS)
# Usually, it should contain all the Rocky Linux repositories we are interested in querying to check if the package is published

DNF_REPOFILE="""

# Rocky 8 sources, all repos:
[Rocky8_BaseOS_Source]
name=Rocky8_BaseOS_Source
baseurl=http://dl.rockylinux.org/pub/rocky/8/BaseOS/source/tree/
gpgcheck=0
enabled=0

[Rocky8_AppStream_Source]
name=Rocky8_AppStream_Source
baseurl=http://dl.rockylinux.org/pub/rocky/8/AppStream/source/tree/
gpgcheck=0
enabled=0

[Rocky8_PowerTools_Source]
name=Rocky8_PowerTools_Source
baseurl=http://dl.rockylinux.org/pub/rocky/8/PowerTools/source/tree/
gpgcheck=0
enabled=0

[Rocky8_extras_Source]
name=Rocky8_extras_Source
baseurl=http://dl.rockylinux.org/pub/rocky/8/extras/source/tree/
gpgcheck=0
enabled=0

[Rocky8_plus_Source]
name=Rocky8_plus_Source
baseurl=http://dl.rockylinux.org/pub/rocky/8/plus/source/tree/
gpgcheck=0
enabled=0

[Rocky8_HighAvailability_Source]
name=Rocky8_HighAvailability_Source
baseurl=http://dl.rockylinux.org/pub/rocky/8/HighAvailability/source/tree/
gpgcheck=0
enabled=0

[Rocky8_ResilientStorage_Source]
name=Rocky8_ResilientStorage_Source
baseurl=http://dl.rockylinux.org/pub/rocky/8/ResilientStorage/source/tree/
gpgcheck=0
enabled=0

[Rocky8_RT_Source]
name=Rocky8_RT_Source
baseurl=http://dl.rockylinux.org/pub/rocky/8/RT/source/tree/
gpgcheck=0
enabled=0

"""
