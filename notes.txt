BUGFIX: check for existence of dictionary package in rockyRpmSource
(ex: if dotnet5.0 not in packagelist, then rockyRpmSource['dotnet5.0'] won't exist, errors out


MQTT protocol (mosquitto client)

mosquitto_sub --cafile ~/.centos-server-ca.cert --cert ~/.centos.cert --key ~/.centos.cert -h mqtt.git.centos.org -p 8883 -t git.centos.org/# --keepalive 60




GIT Notes:
Read remote tags like this: (maybe use python lib?)

git ls-remote --tags  https://git.rockylinux.org/staging/rpms/clevis  imports/c8/clevis-15-1.el8




##############
# Strip module info with regex's in Python:

# (match both the case with a hanging ".1" at the end, and the case where we don't have that.  Need to pass string through both expressions just in case!)

import re

string = 'mailman-2.1.29-12.module+el8.5.0+716+66d1ab43.1'


string = re.sub('module\+.*\+.*\+.*(\..*$)', 'module\\1', string)

string = re.sub('module\+.*\+.*\+.*$', 'module', string)


print(str(string))




# same thing in sed/pcre:

echo 'mailman-2.1.29-12.module+el8.5.0+716+66d1ab43.1'  | sed -E 's/\+.*\+.*\+.*(\..*$)/\1/'  | sed -E 's/\+.*\+.*\+.*$//'


######################



Retrieve git log/tags in lightweight clone:
(maybe in a helper script?)

git init repo
cd repo
git config extensions.partialClone true
git remote add origin https://git.rockylinux.org/staging/rpms/kronosnet
git fetch --filter=blob:none --tags --depth=1 origin

git log --tags --simplify-by-decoration --pretty="format:%ci %d" | grep 'kronosnet-1.18-4.el8_5'

(datestamp for the grep'd tag is listed in the first 2 columns)
