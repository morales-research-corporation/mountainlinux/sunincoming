#!/usr/bin/env python3

# Proposed csv / format for data:
# Timestamp-of-import  |  rpm_name  |  tag-version (end)  |  branch  |  Imported into Rocky?(yes/no)  |   In Rocky repository? (yes/no)  |  

import json, sys, os, time

# import our "setup" (global) variables and config:
import lib.setup as setup

# tag_commit class which holds data for each commit:
from lib.tag_commit import *
from lib.package_list import *
from lib.html_out import *


# tags_list is the main "master" list of objects which have our info
tags_list = []

csv_mode = False


# The program should be called with a list of files to "process": JSON data pulled from MQTT that contains commit info:
inFiles = sys.argv[1:]


def usageExit():
  print("""Usage:  python3 incoming_get.py --csv <List of Files>
  Files list must be space separated.  Use of --csv is optional, default output is an HTML page with table.
  Examples:  python3 incoming_get.py test1.txt test2.txt test3.json
  python3 incoming_get.py --csv test1.txt data/*.txt """)
  sys.exit()
  


if (len(inFiles) == 0):
  print("ERROR: Need to be given at least 1 valid file with JSON commit data from MQTT to process!\n\n")
  usageExit()
  

# Check if files listed exist.  If --csv is listed, then turn on "CSV mode"
for file in inFiles:
  if file == "--csv":
    csv_mode = True
    continue

  if not os.path.exists(file):
    print("ERROR: One or more files in arguments doesn't seem to exist!\n\n")
    usageExit()



# Get a list of all source packages and their versions in the Rocky DNF repos
# It will be available as a dict lookup:  rocky_dnf.sourcePkg["pkgname"] = "name-version-release"
rocky_dnf = package_list()


for file in inFiles:
  
  if file == "--csv":
    continue

  # Open each file for reading, as we loop through them.  We assume each commit/tag time from upstream is the files modification time
  # (they should get rotated once per hour, which gives a good approximation)
  modTime = time.strftime('%Y-%m-%d %H:%M', time.localtime(os.path.getmtime(file)))
  
  f = open(file, 'r')
  json_data = f.readlines()
  f.close()
  
  for line in json_data:
    
    # Skip line if it's obviously too short to have json
    # (useful with hanging whitespace near end of the file)
    if len(line) < 6:
      continue
    
    #load the line into a temporary python dict
    json_tmp = json.loads(line.rstrip())
    
    # We are only looking for commits where a new version gets tagged
    # The branch MUST start with c8, but not be a c8s or a -beta branch, or we dont' care:
    validBranch = False
    if 'tag' in json_tmp:
      
      # Loop through our allowed branch patterns, and see if the branch from JSON contains it:
      for branch in setup.allowed_branches:
        if branch in json_tmp['tag']:
          validBranch = True
      
      # Now loop through our disallowed branch patterns, and see if the branch from JSON has it.
      # Disallowed is the final word, so it's processed last.  If we find a disallowed branch, we don't want this commit!
      for branch in setup.disallowed_branches:
        if branch in json_tmp['tag']:
          validBranch = False

      if validBranch == True:                
        tags_list.append(tag_commit(modTime, json_tmp, rocky_dnf.sourcePkg))

# Write output to STDOUT if we're doing CSV (pipe-separated):
if csv_mode == True:
  # print header:
  print("RHEL Publish Time  |  Package  |  Tagged version  |  Git Branch  |  Imported into Rocky?  |  In Rocky repository?  |  Rocky Build Time ")

  for i in range(0,len(tags_list)):
    tags_list[i].printCommitCsv()

  sys.exit()



# Write output to STDOUT if we're doing HTML (default):

# First, we collect the actual table data into a big string:
tableData = ""

for i in range(0, len(tags_list)):
  tableData += tags_list[i].getCommitHtml()


# Call the html_out class to write our data + html text to STDOUT:
out = html_out(tableData)




